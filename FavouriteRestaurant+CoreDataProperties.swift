//
//  FavouriteRestaurant+CoreDataProperties.swift
//  MC3
//
//  Created by Stefano Di Nunno on 03/03/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//
//

import Foundation
import CoreData

extension FavouriteRestaurant {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FavouriteRestaurant> {
        return NSFetchRequest<FavouriteRestaurant>(entityName: "FavouriteRestaurant")
    }

    @NSManaged public var idRestaurant: String?

}
