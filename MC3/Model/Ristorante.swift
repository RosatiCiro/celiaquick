//
//  Ristorante.swift
//  MC3
//
//  Created by Marco Longobardi on 29/02/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import UIKit
import MapKit
import CloudKit
import CoreLocation

//MARK: - Class Definition
class Ristorante {
    
    /// Qui ci sono tutte le proprietà che ogni ristorante deve avere: recordtype, database e id servono per il collegamento con CloudKit, invece le altre sono i campi che abbiamo
    
    //MARK: - Properties
    static let recordType = "Ristorante"
    private let id: CKRecord.ID
    let name: String
    let location: CLLocation
    let cardPhoto: String
    let database: CKDatabase
    let naplesArea: String
    let menu: String
    let like: Int
    let descriptionPhoto: String
    let description: String
    let category: String
    let address: String
    
    ///Costruttore relativo alla struttura nel Database
    
    //MARK: - Constructor
    init?(record: CKRecord, database: CKDatabase) {
        guard
            let name = record["name"] as? String,
            let location = record["location"] as? CLLocation,
            let naplesArea = record["naplesArea"] as? String,
            let menu = record["menu"] as? String,
            let like = record["like"] as? Int,
            let description = record["description"] as? String,
            let category = record["category"] as? String,
            let address = record["address"] as? String,
            let cardPhoto = record["cardPhoto"] as? String,
            let descriptionPhoto = record["descriptionPhoto"] as? String
            else { return nil }
        
        id = record.recordID
        self.name = name
        self.location = location
        self.naplesArea = naplesArea
        self.menu = menu
        self.like = like
        self.description = description
        self.category = category
        self.address = address
        self.cardPhoto = cardPhoto
        self.descriptionPhoto = descriptionPhoto
        self.database = database
    }
    
    //MARK: - Function used to sort restaurant for distance
    func distance(to location: CLLocation) -> CLLocationDistance {
        return location.distance(from: self.location)
    }
    
}

//MARK: - Extension
extension Ristorante: Hashable {
    static func == (lhs: Ristorante, rhs: Ristorante) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
