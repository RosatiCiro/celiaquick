//
//  Artwork.swift
//  MC3
//
//  Created by Christian Varriale on 27/02/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import Foundation
import MapKit
import Contacts

class Artwork: NSObject, MKAnnotation {
    
    ///Tra le proprietà troviamo il titolo del locale, un advertsiment, una discipline relativa al tipo di locale al quale assegnare un colore ed infine le coordinate per formare l'artwork
    //MARK: - Properties
    var title: String?
    var locationName: String = "Tap the Icon and Go on Maps"
    var discipline: String = ""
    var coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D()
    
    var markerTintColor: UIColor  {
        switch discipline {
        case "Food":
            return #colorLiteral(red: 0.968627451, green: 0.5764705882, blue: 0.1176470588, alpha: 1)
        case "Shop":
            return #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        case "Drink":
            return #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        default:
            return .green
        }
    }
    
    //MARK: Constructor
    init(title: String, discipline: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.discipline = discipline
        self.coordinate = coordinate
        
        super.init()
    }
    
    ///Creazione del MapItem che mi farà collegare a Maps
    // Annotation right callout accessory opens this mapItem in Maps app
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: locationName]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
    
}
