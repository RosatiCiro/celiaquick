//
//  Model.swift
//  MC3
//
//  Created by Marco Longobardi on 29/02/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import Foundation
import CloudKit

//MARK: - Class Definition
class Model {
    
    /// Strutture dati base per collegarsi al Database
    // MARK: - iCloud Info
    let container: CKContainer
    let publicDB: CKDatabase
    
    ///Il primo vettore (ristoranti) conterrà tutti i ristoranti, mentre il secondo (filteredRist) conterrà quelli che vengono filtrati localmente e non con query online per risparmiare dati. Poi troviamo currentModel che definisce l'istanza di questa classe. Due variabili private usate per settare i filtri per l'area e la categoria.
    
    // MARK: - Properties
    private(set) var ristoranti: [Ristorante] = []
    private(set) var filteredRist: [Ristorante] = []
    static var currentModel = Model()
    
    private var areaFilter: String = ""
    private var categoryFilter: String = ""
    
    ///Costruttore di Default per il container e il publicDB
    //MARK: - Constructor
    init() {
        container = CKContainer.default()
        publicDB = container.publicCloudDatabase
        
    }
    
    ///Pulisco il vettore dei filtrati, dopodichè analizzo il vettore di tutti i ristoranti vedendo i tre casi:
    ///1) filtro dell'area selezionato ( != " " ) se vero passo a 2) altrimenti controllo che la categoria sia stata messa
    ///2) filtro della categoria ( != " " ) se vero, passo a trovare tutti i ristoranti che rispettano area e categoria e li aggiungo al vettore dei filtrati, altrimenti controllo che l'area sia stata selezionata
    //MARK: - Query Function to Retrieve Data
    func updateFilter () {
        filteredRist = []
        for i in 0...self.ristoranti.count - 1 {
            if (self.areaFilter != ""){
                if (self.categoryFilter != ""){
                    if (self.ristoranti[i].naplesArea == self.areaFilter && self.ristoranti[i].category == self.categoryFilter){
                        filteredRist.append(self.ristoranti[i])
                        
                    }
                }
                else {
                    if(self.ristoranti[i].naplesArea == self.areaFilter){
                        filteredRist.append(self.ristoranti[i])
                    }
                }
            }
            else {
                if(self.ristoranti[i].category == self.categoryFilter){
                    filteredRist.append(self.ristoranti[i])
                }
            }
        }
    }
    
    ///funzione che ritorna un vettore di ristoranti, che in base ai filtri può essere quello totale o quello dei filtrati
    func getArray () -> [Ristorante] {
        if (self.areaFilter == "" && self.categoryFilter == ""){
            return ristoranti
        }
        else {
            return filteredRist
        }
    }
    
    ///funzione usate per fare la query sul db, che si basa sul predicate ( condizioni logiche ) nel nostro caso True ossia tutti i valori pieni. La query viene effettuata sul tipo di record chiamato Restaurant. Il tutto viene ordinato in base alla key location, che prende il valore dal Singleton.
    @objc func refresh (_ userLoc: CLLocation , _ completion: @escaping (Error?) -> Void) {
        
        let predicate = NSPredicate(value: true)
        
        let query = CKQuery(recordType: "Restaurant", predicate: predicate)
        
        query.sortDescriptors = [CKLocationSortDescriptor(key: "location", relativeLocation: LocationSingleton.sharedInstance.lastLocation)]
        
        ristoranti(forQuery: query, completion)
    }
    
    ///Funzione di connessione al DB, che avviene in modo Async. I risultati ottenuti (ristoranti) vengono ordinati in base alle loro distanze sempre attraverso l'utilizzo del Singleton, sfruttando la last location
    //MARK: - Connection DB CloudKit
    private func ristoranti(forQuery query: CKQuery, _ completion: @escaping (Error?) -> Void) {
        
        publicDB.perform(query, inZoneWith: CKRecordZone.default().zoneID){ [weak self] results, error in
            
            guard let self = self else {return}
            if let error = error {
                DispatchQueue.main.async {
                    completion(error)
                }
                return
            }
            
            guard let results = results else {return}
            self.ristoranti = results.compactMap {
                Ristorante(record: $0, database: self.publicDB)
            }
            
            self.ristoranti.sort(by: { $0.distance(to: LocationSingleton.sharedInstance.lastLocation) < $1.distance(to: LocationSingleton.sharedInstance.lastLocation) })
            
            DispatchQueue.main.async{
                completion(nil)
            }
            
        }
        
    }
    
    ///Funzioni per il set e get dei filtri: se il filtro è vuoto e diverso dal parametro in ingresso, allora lo assegno. Altrimenti se il filtro è vuoto e il parametro è lo stesso (vuol dire che l'ho riselezionato), allora lo resetto. Altrimenti lo assegno.
    //MARK: - Filter Function
    @objc func setAreaFilter( area: String) -> Void  {
        if(areaFilter != "" && areaFilter != area){
            areaFilter = area
        }
        else if (areaFilter != "" && areaFilter == area){
            areaFilter = ""
        }
        else {
            areaFilter = area
        }
    }
    
    @objc func setCategoryFilter( category: String) -> Void {
        if(categoryFilter != "" && categoryFilter != category){
            categoryFilter = category
        }
        else if (categoryFilter != "" && categoryFilter == category){
            categoryFilter = ""
        }
        else {
            categoryFilter = category
        }
        
    }
    
    ///Funzioni di get del valore immagazzinato dal categoryFilter e areaFilter
    func getCategoryFilter() -> String {
        return self.categoryFilter
    }
    
    func getAreaFilter() -> String {
        return self.areaFilter
    }
    
}
