//
//  FavoritesViewController.swift
//  MC3
//
//  Created by Christian Varriale on 18/02/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import UIKit
import CoreLocation

//MARK: - Class Definition
class FavoritesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    ///Vettore contenente i ristoranti salvati nei preferiti del CoreData
    var favouriteRistaurants: [FavouriteRestaurant] = []
    
    @IBOutlet weak var TableView: UITableView!
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate;
        
        TableView.backgroundColor = UIColor.white
        
        favouriteRistaurants = CoreDataController.sharedIstance.getFavouriteRestaurants()!
        
    }
    
    //MARK: - IBACtion
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension FavoritesViewController {
    //MARK: - Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favouriteRistaurants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavoritesCell") as? FavouriteCell
        for i in 0...Model.currentModel.ristoranti.count - 1{
            if Model.currentModel.ristoranti[i].name == self.favouriteRistaurants[indexPath.row].idRestaurant{
                cell?.restaurant = Model.currentModel.ristoranti[i]
                return cell!
            }
            
        }
        
        return UITableViewCell.init()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
}

//MARK: - Extension View Controller for Push Restaurant to Detail View
extension FavoritesViewController {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "DetailView", bundle: nil)
        let detailViewController = storyBoard.instantiateViewController(withIdentifier : "Detail") as! DetailViewController
        
        let cell = tableView.cellForRow(at: indexPath) as? FavouriteCell
        detailViewController.ristorante = cell?.restaurant
        
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
}
