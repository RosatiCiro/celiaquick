//
//  DetailViewController.swift
//  MC3
//
//  Created by Ciro Rosati on 19/02/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: UIViewController {
    
    //MARK: - Properties
    let headerViewMaxHeight: CGFloat = 480
    let headerViewMinHeight: CGFloat = 44 + UIApplication.shared.statusBarFrame.height
    
    var ristorante: Ristorante?
    
    let regionRadius: CLLocationDistance = 100
    
    var locationManager = CLLocationManager()
    
    //MARK: - IBOutlet
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var imagView: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var addr: UILabel!
    @IBOutlet weak var descr: UITextView!
    @IBOutlet weak var menu: UITextView!
    
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    
    @IBOutlet weak var headerView: UIView!
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate;
        
        //MAP
        structMap()
        centerMapOnLocation(location: ristorante!.location)
        
        mapView.register(ArtworkMarkerView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        let artwork = Artwork(title: self.ristorante!.name, discipline: (self.ristorante?.category)!, coordinate: (self.ristorante?.location.coordinate)!)
        
        mapView.addAnnotation(artwork)
        
        mapView.delegate = self
        
        setupLocationManager()
        
        imagView?.image = UIImage(named: ristorante!.descriptionPhoto)
        
        name.text = ristorante?.name
        addr.text = ristorante?.address
        descr.text = ristorante?.description
        menu.text = ristorante?.menu
        
        if CoreDataController.sharedIstance.getFavouriteRestaurant(idRestaurant: name.text!) == nil {
            favouriteButton.isSelected = false
        }else {
            favouriteButton.isSelected = true
        }
        
//        headerView.alpha = 0.6
        headerView.alpha = 1

    }
    
    //MARK: - IBAction
    @IBAction func favouriteButtonPressed(_ sender: UIButton) {
        if sender.isSelected {
            // remove
            CoreDataController.sharedIstance.deleteFavouriteRestaurant(idRestaurant: name.text!)
            
        }else {
            // add
            CoreDataController.sharedIstance.addFavouriteRestaurant(idRestaurant: name.text!)
            
        }
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func likeButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        likeButton.isUserInteractionEnabled = false
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        if favouriteButton.isSelected{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    //MARK: - Function MAP
    func structMap(){
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = false
        mapView.isRotateEnabled = false
        mapView.mapType = .mutedStandard
        mapView.layer.cornerRadius = 20
        
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    //MARK: - Function Header View
    
    //INCREMENTO E DECREMENTO da alpha 0.6 a alpha 1 e viceversa
//    func decrementColorAlpha(viewContent: UIView, offset: CGFloat) {
//        if self.headerView.alpha <= 1 {
//            let alphaOffset = (offset/500)/85
//            self.headerView.alpha += alphaOffset
//        }
//    }
//
//    func incrementColorAlpha(viewContent: UIView, offset: CGFloat) {
//        if self.headerView.alpha >= 0.6 {
//            let alphaOffset = (offset/200)/85
//            self.headerView.alpha -= alphaOffset
//        }
//    }
    
    //INCREMENTO E DECREMENTO da alpha 1 a alpha 0.6 e viceversa
    func decrementColorAlpha(viewContent: UIView, offset: CGFloat) {
        if self.headerView.alpha <= 1 {
            let alphaOffset = (offset/500)/85
            self.headerView.alpha += alphaOffset
        }
    }

    func incrementColorAlpha(viewContent: UIView, offset: CGFloat) {
        if self.headerView.alpha >= 0.6 {
            let alphaOffset = (offset/200)/85
            self.headerView.alpha -= alphaOffset
        }
    }
    
    func animateHeader() {
        self.headerViewHeightConstraint.constant = 480
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
            }, completion: nil)
    }
}

// MARK: - UIScrollViewDelegate
extension DetailViewController: UIScrollViewDelegate {
    
    //BASE
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let y: CGFloat = scrollView.contentOffset.y
//        let newHeaderViewHeight: CGFloat = headerViewHeightConstraint.constant - y
//
//        if newHeaderViewHeight > headerViewMaxHeight {
//            headerViewHeightConstraint.constant = headerViewMaxHeight
//        } else if newHeaderViewHeight < headerViewMinHeight {
//            headerViewHeightConstraint.constant = headerViewMinHeight
//        } else {
//            headerViewHeightConstraint.constant = newHeaderViewHeight
//            scrollView.contentOffset.y = 0 // block scroll view
//        }
//    }
    
    //RIMBALZO SEMPLICE
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y: CGFloat = scrollView.contentOffset.y
        let newHeaderViewHeight: CGFloat = headerViewHeightConstraint.constant - y
        
        if scrollView.contentOffset.y < 0{
            
            self.headerViewHeightConstraint.constant += abs(scrollView.contentOffset.y)
            
        }else if scrollView.contentOffset.y > 0 && self.headerViewHeightConstraint.constant >= 88{
            
            if newHeaderViewHeight > headerViewMaxHeight {
                
                headerViewHeightConstraint.constant = headerViewMaxHeight
                
            } else if newHeaderViewHeight < headerViewMinHeight {
                
                headerViewHeightConstraint.constant = headerViewMinHeight
                
            } else {
                
                headerViewHeightConstraint.constant = newHeaderViewHeight
                scrollView.contentOffset.y = 0 // block scroll view
                
            }
            
        }
        print(headerView.alpha)
    }
    
    //MODIFICATA CON RIMBALZO -> da alpha 1 a alpha 0.6 e viceversa
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let y: CGFloat = scrollView.contentOffset.y
//        let newHeaderViewHeight: CGFloat = headerViewHeightConstraint.constant - y
//
//        if scrollView.contentOffset.y < 0{
//
//            self.headerViewHeightConstraint.constant += abs(scrollView.contentOffset.y)
//            self.decrementColorAlpha(viewContent: imagView, offset: self.headerViewHeightConstraint.constant)
//
//        }else if scrollView.contentOffset.y > 0 && self.headerViewHeightConstraint.constant >= 88{
//
//            self.incrementColorAlpha(viewContent: imagView, offset: scrollView.contentOffset.y)
//
//            if newHeaderViewHeight > headerViewMaxHeight {
//                
//                headerViewHeightConstraint.constant = headerViewMaxHeight
//
//            } else if newHeaderViewHeight < headerViewMinHeight {
//
//                headerViewHeightConstraint.constant = headerViewMinHeight
//
//            } else {
//                
//                headerViewHeightConstraint.constant = newHeaderViewHeight
//                scrollView.contentOffset.y = 0 // block scroll view
//
//            }
//        }
//        print(headerView.alpha)
//    }
    
    //MODIFICATA CON RIMBALZO -> da alpha 0.6 a alpha 1 e viceversa
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let y: CGFloat = scrollView.contentOffset.y
//        let newHeaderViewHeight: CGFloat = headerViewHeightConstraint.constant - y
//
//        if scrollView.contentOffset.y < 0{
//
//            self.headerViewHeightConstraint.constant += abs(scrollView.contentOffset.y)
//            self.incrementColorAlpha(viewContent: headerView, offset: self.headerViewHeightConstraint.constant)
//
//        }else if scrollView.contentOffset.y > 0 && self.headerViewHeightConstraint.constant >= 88{
//
//            self.decrementColorAlpha(viewContent: headerView, offset: scrollView.contentOffset.y)
//
//            if newHeaderViewHeight > headerViewMaxHeight {
//
//                headerViewHeightConstraint.constant = headerViewMaxHeight
//
//            } else if newHeaderViewHeight < headerViewMinHeight {
//
//                headerViewHeightConstraint.constant = headerViewMinHeight
//
//            } else {
//
//                headerViewHeightConstraint.constant = newHeaderViewHeight
//                scrollView.contentOffset.y = 0 // block scroll view
//
//            }
//        }
//        print(headerView.alpha)
//    }
    
    //RIFERIMENTO DA CUI HO COPIATO
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//            if scrollView.contentOffset.y < 0 {
//
//                self.headerViewHeightConstraint.constant += abs(scrollView.contentOffset.y)
//                self.incrementColorAlpha(viewContent: headerView, offset: self.headerViewHeightConstraint.constant)
//
//            } else if scrollView.contentOffset.y > 0 && self.headerViewHeightConstraint.constant >= 88 {
//
//                self.headerViewHeightConstraint.constant -= scrollView.contentOffset.y/100
//                self.decrementColorAlpha(viewContent: headerView, offset: scrollView.contentOffset.y)
//
//                if self.headerViewHeightConstraint.constant < 88 {
//                    self.headerViewHeightConstraint.constant = 88
//                }
//            }
//        }
        
        func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
            if self.headerViewHeightConstraint.constant > 480 {
                animateHeader()
            }
        }
        
        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            if self.headerViewHeightConstraint.constant > 480 {
                animateHeader()
            }
        }
}

//MARK: - Present visual indicator of the annotation
extension DetailViewController: MKMapViewDelegate {
    
    // to use maps with the annotation
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! Artwork
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
    
}

extension DetailViewController: CLLocationManagerDelegate {
    
    func setupLocationManager() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        
        // Only look at locations within a 0.5 km radius.
        //    locationManager.distanceFilter = 500.0
        locationManager.delegate = self
        
        CLLocationManager.authorizationStatus()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)  {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
        default:
            // Do nothing.
            print("Other status")
        }
    }
}

