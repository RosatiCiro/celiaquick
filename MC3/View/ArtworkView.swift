//
//  ArtworkView.swift
//  MC3
//
//  Created by Christian Varriale on 27/02/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import Foundation
import MapKit

//MARK: - Class Definition

class ArtworkMarkerView: MKMarkerAnnotationView {
    
    ///creazione della variabile annotation, usando il willSet poichè il valore della proprietà è settato al di fuori del contesto di inizializzazione. creo l'artwork, ci metto il button Maps per collegarmi alle mappe, una label con l'indicazione.
    //MARK: - Properties
    override var annotation: MKAnnotation? {
        willSet {
            
            guard let artwork = newValue as? Artwork else { return }
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            
            let mapsButton = UIButton(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 30, height: 30)))
            mapsButton.setBackgroundImage(UIImage(named: "Maps-icon"), for: UIControl.State())
            rightCalloutAccessoryView = mapsButton
            
            let detailLabel = UILabel()
            detailLabel.numberOfLines = 0
            detailLabel.font = detailLabel.font.withSize(12)
            detailLabel.text = artwork.locationName
            detailCalloutAccessoryView = detailLabel
            
            markerTintColor = artwork.markerTintColor
            glyphText = String(artwork.discipline.first!)
            
        }
    }
}
