//
//  CoreDataController.swift
//  MC3
//
//  Created by Stefano Di Nunno on 03/03/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataController {
    
    static let sharedIstance = CoreDataController() // Singleton Pattern
    private var context: NSManagedObjectContext
    
    private init() {
        let application = UIApplication.shared.delegate as! AppDelegate
        self.context = application.persistentContainer.viewContext
    }

    func addFavouriteRestaurant(idRestaurant: String) -> FavouriteRestaurant? {
        if getFavouriteRestaurant(idRestaurant: idRestaurant) != nil {
            // Session already in memory, returning nil
            return nil
        }
        
        let entityFav = NSEntityDescription.entity(forEntityName: "FavouriteRestaurant", in: self.context)
        let newFav    = FavouriteRestaurant(entity: entityFav!, insertInto: context)
        
        newFav.idRestaurant = idRestaurant
        
        self.saveContext()
        return newFav
    }

    func deleteFavouriteRestaurant(idRestaurant: String) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "FavouriteRestaurant")
        request.predicate = NSPredicate(format: "idRestaurant = %@", argumentArray: [idRestaurant])
        request.returnsObjectsAsFaults = false
               
        do {
            let result = try self.context.fetch(request) as NSArray
            for data in result {
                context.delete(data as! NSManagedObject)
                self.saveContext()
            }
        } catch let error {
            print(error)
        }
    }
    
    func getFavouriteRestaurants() -> [FavouriteRestaurant]? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "FavouriteRestaurant")
        request.returnsObjectsAsFaults = false
        
        do {
            let result: NSArray = try self.context.fetch(request) as NSArray
            return result as? [FavouriteRestaurant]
            
        } catch let error {
            print(error)
        }
        
        return nil
    }
    
    func getFavouriteRestaurant(idRestaurant: String) -> FavouriteRestaurant? {
        var fav: FavouriteRestaurant?
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "FavouriteRestaurant")
        request.predicate = NSPredicate(format: "idRestaurant = %@", idRestaurant)
        request.returnsObjectsAsFaults = false
        
        do {
            let result: NSArray = try self.context.fetch(request) as NSArray
            
            switch result.count {
            case 0:
                // Session doesn't exist
                return nil
                
            case 1:
                // Session found
                fav = result[0] as? FavouriteRestaurant
                return fav!
                                
            default:
                return nil
            }
            
        } catch let error {
            print(error)
        }
        
        return nil
    }
    
    private func saveContext() {
        do {
            try self.context.save()
        } catch let error {
            print(error)
        }
    }
}
