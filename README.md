CELIAQuick

Technologies used: UIKit, CoreData, CloudKit, MapKit, CoreLocation

CELIAQuick is an app that improves the life of celiac people by providing them a selection of places where to buy, eat and drink products in Naples. We gathered personally all the information, making them available to the app using iCloud.

The app is almost ready to be launched on the store, this is the TestFlight link: https://testflight.apple.com/join/BPJ3Mjbw

During this challenge, I worked as a front-end developer. I managed all the restaurants infos, populating our databases, also I met some restaurant's owners to speak about the app to understand if they respected our qualities standards.